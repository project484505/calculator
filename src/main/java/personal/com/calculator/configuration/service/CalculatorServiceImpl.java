package personal.com.calculator.configuration.service;

import org.springframework.stereotype.Component;
import personal.com.calculator.configuration.Commons.SubstractRequestDto;

@Component
public class CalculatorServiceImpl implements CalculatorService {

    @Override
    public int add(int a, int b) {

        return a + b;
    }

    @Override
    public int substract(SubstractRequestDto substractRequestDto) {
        return 0;
    }

//    @Override
//    public int substract(SubstractRequestDto substractRequestDto) {
//        return substractRequestDto.getFirstPara() - substractRequestDto.getSecondPata();
//    }
}
