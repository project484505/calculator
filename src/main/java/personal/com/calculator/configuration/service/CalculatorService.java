package personal.com.calculator.configuration.service;

import personal.com.calculator.configuration.Commons.SubstractRequestDto;

public interface CalculatorService {
    int add(int a, int b);

    int substract(SubstractRequestDto substractRequestDto);
}
