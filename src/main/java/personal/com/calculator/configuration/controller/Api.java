package personal.com.calculator.configuration.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import personal.com.calculator.configuration.service.CalculatorService;

@RestController
public class Api {

    private final CalculatorService calculatorService;

    public Api(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    @RequestMapping("/add")
    public Integer add(){
        return calculatorService.add(45, 89);
    }

    @RequestMapping("/sub")
    public Integer substract(){
        return calculatorService.add(10, 5);
    }
}
