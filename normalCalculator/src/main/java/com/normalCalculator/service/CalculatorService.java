package com.normalCalculator.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class CalculatorService {

    @Autowired
    private KafkaTemplate<Integer,Integer> kafkaTemplate;
//    private Logger logger = LoggerFactory.getLogger(CalculatorService.class);
    public int add(int a, int b){

        return a+b;
    }
}
