package personal.com.calculator.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletRegistrationBean;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.Ordered;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import personal.com.calculator.configuration.service.CalculatorService;
import personal.com.calculator.configuration.service.CalculatorServiceImpl;

@SpringBootApplication
public class CalculatorLauncher {

	public static void main(String[] args) {
		SpringApplication.run(CalculatorLauncher.class, args);
	}

//	public static final String APP_ROOT = "/app";
//
//	@Bean
//	public DispatcherServletRegistrationBean cartAPI() {
//		DispatcherServlet dispatcherServlet = new DispatcherServlet();
//		AnnotationConfigWebApplicationContext applicationContext = new AnnotationConfigWebApplicationContext();
//		applicationContext.register(CalculatorPublicApiConfig.class);
//		dispatcherServlet.setApplicationContext(applicationContext);
//		DispatcherServletRegistrationBean servletRegistrationBean = new DispatcherServletRegistrationBean(
//				dispatcherServlet, APP_ROOT + "/p/*");
//		servletRegistrationBean.setName("drpCartAPI");
//		servletRegistrationBean.setLoadOnStartup(1);
//		return servletRegistrationBean;
//	}
//
//	@Bean("appRoot")
//	public String getAppRoot() {
//		return APP_ROOT;
//	}
//
//
//	@Bean
//	@Primary
//	public DispatcherServletRegistrationBean dispatcherServletRegistrationBeanRest() {
//		DispatcherServlet dispatcherServlet = new DispatcherServlet(new AnnotationConfigServletWebApplicationContext("personal.com.calculator.controller"));
//		DispatcherServletRegistrationBean dispatcher = new DispatcherServletRegistrationBean(dispatcherServlet , "/p/*");
//		dispatcher.setName("publicDispatcher");
//		dispatcher.setLoadOnStartup(1);
//		dispatcher.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return dispatcher;
//	}
//
//	@Bean
//	public DispatcherServletRegistrationBean dispatcherServletRegistrationBeanJsp() {
//		DispatcherServlet dispatcherServlet = new DispatcherServlet(new AnnotationConfigServletWebApplicationContext("personal.com.calculator.controller"));
//		DispatcherServletRegistrationBean dispatcher = new DispatcherServletRegistrationBean(dispatcherServlet , "/c/*");
//		dispatcher.setName("restrictedDispatcher");
//		dispatcher.setLoadOnStartup(1);
//		dispatcher.setOrder(Ordered.HIGHEST_PRECEDENCE);
//		return dispatcher;
//	}
//
//	@Bean
//	public TomcatServletWebServerFactory servletWebServerFactory() {
//		TomcatServletWebServerFactory tomcatServletWebServerFactory = new TomcatServletWebServerFactory("/cs",8081);
//		return tomcatServletWebServerFactory;
//	}

}
