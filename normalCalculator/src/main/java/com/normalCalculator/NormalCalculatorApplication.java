package com.normalCalculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NormalCalculatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(NormalCalculatorApplication.class, args);
	}

}
